#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import json
from typing import Dict, Tuple
from urllib.parse import urljoin

import requests

default_header = {
    "Content-Type": "application/json"
}


class APIClient:
    """
    This class is used to make API calls.
    """

    def __init__(
            self,
            username: str,
            password: str,
            base_url: str,
    ) -> None:
        self.header_type = {
            "Content-Type": "application/json"
        }
        self.user = username
        self.psw = password
        self.base_url = base_url
        self.auth_data = (self.user, self.psw)

    def build_url(self, url_target: str) -> str:
        """
        Builds url by appending the url target to the base host and port.
        :param url_target: target url to be reached
        :returns: url with format http://base_url:port/url_target
        """
        return urljoin(self.base_url, url_target)

    def post_request(self,
                     api: str,
                     data: Dict=None,
                     params: Dict=None,
                     files: Dict=None,
                     headers: Dict=default_header,
                     data_json: Dict=None) -> Dict:
        """
        Creates POST request with provided api, data and setting necessary headers.
        :param api: target url to be called for creating POST request.
        :param data: dictionary format of data (payload) to create POST request.
        :param params: dictionary format of data for query params to create POST request.
        :param files: the files to be stored to be provided as dictionary. 
        :param headers: additional headers to be set for the POST request.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        post_response = requests.post(api,
                                      auth=self.auth_data,
                                      headers=headers,
                                      data=data,
                                      params=params,
                                      files=files,
                                      json=data_json
                                      )
        return post_response

    def post_document_request(self, api: str, params: Tuple, files: Dict, headers: Dict=None) -> object:
        """
        Creates POST request to the provided api, storing the files with provided params.
        :param api: target url to be called for creating POST request.
        :param params: Tuple of params, in key value pair.
        :param files: the files to be stored to be provided as dictionary.
        :param headers: additional headers to be set for the POST request.
        :returns: response object of the API call.
        """
        api = self.build_url(api)
        response = requests.post(api,
                                 params=params,
                                 files=files,
                                 auth=self.auth_data,
                                 headers=headers)
        return response

    def post_document_request_wo_auth(self, api: str, params: Tuple, files: Dict, headers: Dict=None) -> object:
        """
        Creates POST request to the provided api, storing the files with provided params, excluding the authentication tokens.
        Useful for onboarding solutions, where auth tokens are not needed.
        :param api: target url to be called for creating POST request.
        :param params: Tuple of params, in key value pair.
        :param files: the files to be stored to be provided as dictionary.
        :param headers: additional headers to be set for the POST request.
        :returns: response object of the API call.
        """
        api = self.build_url(api)
        response = requests.post(api,
                                 params=params,
                                 files=files,
                                 headers=headers)
        return response
    
    def put_request(self, api: str, data: Dict, headers: Dict=default_header):
        """
        Creates PUT request to the provided api, storing the data with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: dictionary format of data to create PUT request.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=json.dumps(data)
                                    )
        return put_response.json()

    def put_image_request(self, api: str, data: str, headers: Dict=default_header):
        """
        Creates PUT request to the provided api, storing the image with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: image as raw bytes string.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=data
                                    )
        return put_response.json()

    def get_put_request(self, api: str, data: Dict, headers: Dict=default_header):
        """
        First makes a get request to retrieve the existing data.
        Then updates the received information with data.
        Lastly, creates PUT request to the provided api, updating the data with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: dictionary format of data to create PUT request.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        get_response = self.get_request(api)
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=json.dumps(get_response | data)
                                    )
        return put_response.json()

    def get_request(
            self,
            api: str,  # url at which request is to be sent
    ) -> Dict:
        """
        Creates GET request to retrieve data from the provided API.
        :param api: target url to be called for retrieving data from GET request.
        :returns: dictionary representing the json response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        return response.json()

    def get_document(
            self,
            api: str,  # url at which request is to be sent
    ) -> Dict:
        """
        Creates GET request to retrieve document from the provided API.
        :param api: target url to be called for retrieving data from GET request.
        :returns: dictionary representing the json response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        file_name = response.url.split('/')[-1]
        version = response.url.split('/')[-2]
        file_name_wo_version = file_name.replace(f'-{version}', '')
        document = {
            'content': response.content,
            'file_name': file_name,
            'file_name_wo_version': file_name_wo_version,
            'version': version
        }
        return document

    def get_image(
            self,
            api: str,  # url at which request is to be sent
    ) -> object:
        """
        Retrieves image from the host.
        :param api: target url to be called for retrieving data from GET request.
        :returns: icon image as a bytes string object from response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        return response.content

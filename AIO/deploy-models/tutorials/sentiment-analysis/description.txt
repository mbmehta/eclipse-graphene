Overview description

The Sentiment-analysis-pipeline shows how an AI model can determine the sentiment from text, which can be provided by the user.

Use case example

Imagine the work in the customer service. In this area, the service team receive a large number of messages from customers every day. With the sentiment analysis, the messages can automatically be categorized and prioritised. The solution helps to extract meta-information from the texts and make them useful for the customer service.

Usage

Select the "Sentiment-analysis-pipeline" in the Marketplace or in the Design Studio. It is possible to download the solution or to run it on the Playground for testing purposes. When the solution is deployed in the Playground, select the Web-UI of the databroker and fill in the text which should be analyzed. It has a user interface(UI) that takes the query text from the user and connects to the prediction model. Then go back to the Playground and run the solution once and open the Web-UI (interface) of the model. The results can then be viewed on the Prediction model's UI.

Support

For further construction feel free to reach out to the AI.Lab team ai-lab@iais.fraunhofer.de or directly with the developer of the technology. The developer teams are generelly open for feedback and happy about co-creation opportunities.
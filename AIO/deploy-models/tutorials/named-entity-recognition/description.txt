Overview description

The Named Entity Recognition module (named-entity-recognition) can either predict instances of entities in a given piece of text or train new Deep Learning NER models, using Flair framework. The pipeline predicts entities belonging to one of the 4 classes: Person, Location, Organization or Miscellaneous by entering the text in the given text area field in the ner-model Web-UI. Training new DL-NER models is possible by entering relevant training parameters and your own training datasets in the Web-UI form provided by the ner-trainingconfig module.

Use case example

As an NLP researcher, I want to be able to train multiple NER models with different configurations, so that I can decide which configuration works the best for my data. 

Usage

Select the "named-entity-recognition" solution in the Marketplace or in the Design Studio. It is possible to download the solution or to run it on the Playground for testing purposes. When the solution is deployed in the Playground, select the Web-UI of the ner-model and give a text as input. The result text with the highlighted entities will automatically appear in the result section (text enriched with entity predictions (entity label and confidence score)). If you are interested in training new models, then select the Web-UI of the trainingconfig and fill in the form. Then go back to the Playground and run the solution once. The training will start in the background and it will take some time until it is ready, depending on the size of your dataset, number of epochs, learning rate etc. As long as the training is running, the deployed pipeline will have Status: Running. You can access the new trained model in the Web-UI of SharedFolder module.

Support

The solution is part of the tutorials with developer documentation and source code available. For further construction feel free to reach out to the AI.Lab team ai-lab@iais.fraunhofer.de or directly with the developer of the technology. The developer teams are generally open for feedback and happy about co-creation opportunities.
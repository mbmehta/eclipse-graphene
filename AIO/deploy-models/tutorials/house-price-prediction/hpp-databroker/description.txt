Databroker of the House Price Prediction Pipeline.

It has a WebUI that can be used to feed input to the model that predicts the sale price of a house.
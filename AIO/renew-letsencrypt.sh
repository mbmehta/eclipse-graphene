#!/usr/bin/bash

SCRIPT_PATH=$(realpath $0)
SCRIPT_DIR=$(dirname $SCRIPT_PATH)
export GRAPHENE_USER=$(echo $SCRIPT_PATH | cut -d / -f 3)
echo $GRAPHENE_USER
source ${SCRIPT_DIR}/graphene_env.sh


# executed as graphene user
function clean_ingress() {
  cd $HOME/eclipse-graphene/AIO
  source graphene_env.sh
  if [[ $(helm uninstall -n ${GRAPHENE_NAMESPACE} ${GRAPHENE_NAMESPACE}-nginx-ingress) ]]; then
    echo "Helm release ${GRAPHENE_NAMESPACE}-nginx-ingress uninstalled"
  fi
  if [[ $(kubectl get namespace -n $GRAPHENE_NAMESPACE) ]]; then
    echo "Cleanup any ingress-related resources"
    if [[ $(kubectl delete secret -n $GRAPHENE_NAMESPACE ingress-cert) ]]; then
      echo "Secret ingress-cert deleted"
    fi
    ings=$(kubectl get ingress -n $GRAPHENE_NAMESPACE | awk '/-ingress/{print $1}')
    for ing in $ings; do
      kubectl delete ingress -n $GRAPHENE_NAMESPACE $ing
    done
  fi
}

# executed as graphene user
function install_certs() {
  cd $HOME/eclipse-graphene/AIO
  source graphene_env.sh
  bash $AIO_ROOT/setup_keystore.sh
  bash $AIO_ROOT/../charts/ingress/setup_ingress_controller.sh $GRAPHENE_NAMESPACE $AIO_ROOT/certs/graphene.crt $AIO_ROOT/certs/graphene.key $GRAPHENE_DOMAIN_IP
  bash $AIO_ROOT/ingress/setup_ingress.sh
}

export -f clean_ingress
su $GRAPHENE_USER -c clean_ingress

# wait for cleaning of iptables rules 
sleep 15
echo '***************************************************'
certbot renew -n --force-renewal
cd $AIO_ROOT/certs
cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/privkey.pem graphene.key
chown $GRAPHENE_USER:$GRAPHENE_USER graphene.key
cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/fullchain.pem graphene.crt
chown $GRAPHENE_USER:$GRAPHENE_USER graphene.crt
echo '***************************************************'

export -f install_certs
su $GRAPHENE_USER -c install_certs



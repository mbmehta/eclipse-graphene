apiVersion: apps/v1
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================

# What this is: kubernetes template for Graphene dsce deployment
# How to use:

kind: Deployment
metadata:
  namespace: <GRAPHENE_NAMESPACE>
  name: dsce
  labels:
    app: dsce
spec:
  replicas: 1
  selector:
    matchLabels:
      app: dsce
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: dsce
        <GRAPHENE_SERVICE_LABEL_KEY>: <GRAPHENE_ACUCOMPOSE_SERVICE_LABEL>
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: <GRAPHENE_SERVICE_LABEL_KEY>
                operator: NotIn
                values:
                - <GRAPHENE_ACUCOMPOSE_SERVICE_LABEL>
            topologyKey: "kubernetes.io/node"
      imagePullSecrets:
      - name: graphene-registry
      containers:
      - name: dsce
        image: <DESIGNSTUDIO_IMAGE>
        command: ["/bin/sh", "-c"]
        args:
        - set -x;
          cd maven;
          java $JAVA_OPTS -Dhttp.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttp.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttp.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Dhttps.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttps.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttps.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Djava.security.egd=file:/dev/./urandom -jar *.jar
        env:
        - name: GRAPHENE_HTTP_NON_PROXY_HOSTS
          value: "<GRAPHENE_HTTP_NON_PROXY_HOSTS>|cds-service"
        - name: GRAPHENE_HTTP_PROXY_HOST
          value: "<GRAPHENE_HTTP_PROXY_HOST>"
        - name: GRAPHENE_HTTP_PROXY_PORT
          value: "<GRAPHENE_HTTP_PROXY_PORT>"
        - name: JAVA_OPTS
          value: "-Xms128m -Xmx512m"
        - name: SPRING_APPLICATION_JSON
          value: '{
            "logging": {
              "level": {
                "root": "INFO"
              }
            },
            "server": {
              "port": 8088
            },
            "gdmJarName":"gdmservice-1.2.0-SNAPSHOT.jar",
            "dateformat": "yyyy-MM-dd-HH-mm-ss-SSS",
	    "modelMatchingExcludedMessages": "",
            "solutionResultsetSize": "2000",
            "cdsCheckInterval": 20000,
            "cdsCheckAttempt": 10,
            "imagedatabrokerURI": "<DATABROKER_ZIPBROKER_IMAGE>",
            "csvdatabrokerURI": "<DATABROKER_CSVBROKER_IMAGE>",
            "sqldatabrokerURI": "<DATABROKER_SQLBROKER_IMAGE>",
            "jsondatabrokerURI": "NA",
            "sqldatabrokerURI": "NA",
            "lib":"/maven/",
            "tosca": {
              "outputfolder": "/tmp/output/"
            },
            "nexus": {
              "nexusendpointurl": "http://<GRAPHENE_NEXUS_HOST>:<GRAPHENE_NEXUS_API_PORT>/<GRAPHENE_NEXUS_MAVEN_REPO_PATH>/<GRAPHENE_NEXUS_MAVEN_REPO>/",
              "nexusproxy": "",
              "nexuspassword": "<GRAPHENE_NEXUS_RW_USER_PASSWORD>",
              "nexususername": "<GRAPHENE_NEXUS_RW_USER>",
              "nexusgroupid": "<GRAPHENE_NEXUS_GROUP>"
            },
            "cmndatasvc": {
              "cmndatasvcendpoinurl": "http://<GRAPHENE_CDS_HOST>:<GRAPHENE_CDS_PORT>/ccds",
              "cmndatasvcuser": "<GRAPHENE_CDS_USER>",
              "cmndatasvcpwd": "<GRAPHENE_CDS_PASSWORD>"
            },
            "docker": {
              "host": "<GRAPHENE_DOCKER_API_HOST>",
              "port": "<GRAPHENE_DOCKER_API_PORT>",
              "config": "/docker_host/.docker",
              "registry": {
                "url": "http://<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT>/",
                "username": "<GRAPHENE_DOCKER_REGISTRY_USER>",
                "password": "<GRAPHENE_DOCKER_REGISTRY_PASSWORD>",
                "email": "<GRAPHENE_ADMIN_EMAIL>"
              },
              "tls": {"verify": "false"},
              "api": {"version": "1.23"},
              "imagetag": {
                "prefix": "<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT>"
              },
              "max_total_connections": "1",
              "max_per_route_connections": "1"
            }
          }'
        ports:
        - containerPort: 8088
        volumeMounts:
        - mountPath: /maven/logs
          name: logs
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
         claimName: <GRAPHENE_ACUCOMPOSE_SERVICE_LABEL>

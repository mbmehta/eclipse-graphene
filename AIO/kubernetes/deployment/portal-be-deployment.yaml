apiVersion: apps/v1
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================

# What this is: kubernetes template for Graphene portal-be deployment
# How to use:

kind: Deployment
metadata:
  namespace: <GRAPHENE_NAMESPACE>
  name: portal-be
  labels:
    app: portal-be
spec:
  replicas: 1
  selector:
    matchLabels:
      app: portal-be
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: portal-be
        <GRAPHENE_SERVICE_LABEL_KEY>: <GRAPHENE_PORTAL_SERVICE_LABEL>
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: <GRAPHENE_SERVICE_LABEL_KEY>
                operator: NotIn
                values:
                - <GRAPHENE_PORTAL_SERVICE_LABEL>
            topologyKey: "kubernetes.io/node"
      imagePullSecrets:
      - name: graphene-registry
      containers:
      - name: portal-be
        image: <PORTAL_BE_IMAGE>
        command: ["/bin/sh", "-c"]
        args:
        - set -x;
          cd maven;
          java $JAVA_OPTS -Dhttp.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttp.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttp.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Dhttps.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttps.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttps.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Djava.security.egd=file:/dev/./urandom -jar *.jar
        env:
        - name: GRAPHENE_HTTP_NON_PROXY_HOSTS
          value: "<GRAPHENE_HTTP_NON_PROXY_HOSTS>|docker-dind-service|cds-service|deployment-client-service|onboarding-service|msg-service|sv-scanning-service|lpe-service|lum-service"
        - name: GRAPHENE_HTTP_PROXY_HOST
          value: "<GRAPHENE_HTTP_PROXY_HOST>"
        - name: GRAPHENE_HTTP_PROXY_PORT
          value: "<GRAPHENE_HTTP_PROXY_PORT>"
        - name: JAVA_OPTS
          value: "-Xms512m -Xmx1024m"
        - name: SPRING_APPLICATION_JSON
          value: '{
            "logging": {
              "level": {
                "root": "INFO"
              }
            },
            "server": {
              "port": 8083,
              "contextPath": "/"
            },
            "proxy": {
              "host": "<GRAPHENE_HTTP_PROXY>",
              "port": "<GRAPHENE_HTTP_PROXY_PORT>",
              "protocol": "<GRAPHENE_HTTP_PROXY_PROTOCOL>"
            },
            "docker": {
              "host": "<GRAPHENE_DOCKER_API_HOST>",
              "port": "<GRAPHENE_DOCKER_API_PORT>",
              "registry": {
                "url": "http://<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT>/",
                "username": "<GRAPHENE_DOCKER_REGISTRY_USER>",
                "password": "<GRAPHENE_DOCKER_REGISTRY_PASSWORD>",
                "email": "<GRAPHENE_ADMIN_EMAIL>"
              },
              "imagetag": {
                "prefix": "<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT><GRAPHENE_DOCKER_IMAGETAG_PREFIX>"
              }
            },
            "nexus": {
              "url": "http://<GRAPHENE_NEXUS_HOST>:<GRAPHENE_NEXUS_API_PORT>/<GRAPHENE_NEXUS_MAVEN_REPO_PATH>/<GRAPHENE_NEXUS_MAVEN_REPO>/",
              "proxy": "",
              "password": "<GRAPHENE_NEXUS_RW_USER_PASSWORD>",
              "username": "<GRAPHENE_NEXUS_RW_USER>",
              "groupId": "<GRAPHENE_NEXUS_GROUP>"
            },
            "cdms": {
              "client": {
                "url": "http://<GRAPHENE_CDS_HOST>:<GRAPHENE_CDS_PORT>/ccds",
                "username": "<GRAPHENE_CDS_USER>",
                "password": "<GRAPHENE_CDS_PASSWORD>"
              }
            },
            "qanda": {
              "url": "https://gitlab.eclipse.org/eclipse/graphene/tutorials"
            },
            "concurrency" : {
              "async" : {
                "core-pool-size" : 10,
                "max-pool-size" : 50,
                "queue-capacity" : 10000
              }
            },
            "elk" : {
              "url" : "http://<GRAPHENE_ELK_DOMAIN>:30960/elkclient"
            },
            "k8_deploy" : {
              "url" : "http://deployment-client-service:8337"
            },
            "lum": {
              "url": "http://lum-service:8080"
            },
            "license_profile": {
              "url": "https://<GRAPHENE_DOMAIN>/license-profile-editor/index.html"
            },
            "microservice": {
              "url": "http://msg-service:8336/microservice-generation/v2/generateMicroservice"
            },
            "doc": {
              "url": "https://gitlab.eclipse.org/eclipse/graphene/tutorials"
            },
            "onboarding": {
              "push": {
                "model": {
                  "url": "http://onboarding-service:8090/onboarding-app/v2/models",
                  "dcae_url": "http://onboarding-service:8090/onboarding-app/v2/dcae_models"
                },
                "advancedmodel": {
                  "url": "http://onboarding-service:8090/onboarding-app/v2/advancedModel"
                }
              },
              "cliPushUrl": "https://<GRAPHENE_ORIGIN><GRAPHENE_ONBOARDING_CLIPUSHAPI>",
              "cliAuthUrl": "https://<GRAPHENE_ORIGIN><GRAPHENE_ONBOARDING_CLIAUTHAPI>",
              "tokenmode": "<GRAPHENE_ONBOARDING_TOKENMODE>"
            },
            "model": {
              "storage": {
                "folder": {
                  "name": "/grapheneWebOnboarding"
                }
              }
            },
            "portal": {
              "feature": {
                "publishSelfRequestEnabled": "<GRAPHENE_PORTAL_PUBLISH_SELF_REQUEST_ENABLED>",
                "enablePublication": "<GRAPHENE_PORTAL_ENABLE_PUBLICATION>",
                "validateModel": "false",
                "email_service": "<GRAPHENE_EMAIL_SERVICE>",
                "mail": {
                   "sender": "graphene@<GRAPHENE_DOMAIN>"
                },
                "cas_enabled": "<GRAPHENE_CAS_ENABLE>",
                "cas": {
                  "login": "https://ecas.ec.europa.eu/cas/login?service=",
                  "logout": "https://ecas.ec.europa.eu/cas/logout"
                },
                "download_bufferSize": 8,
                "signup_enabled": "true",
                "verifyAccount": "<GRAPHENE_VERIFY_ACCOUNT>",
                "verifyToken": {
                  "exp_time": <GRAPHENE_TOKEN_EXP_TIME>
                },
                "loginExpire" : {
                  "duration" : "8Y"
                },
                "sv": {
                  "enabled": "<GRAPHENE_ENABLE_SECURITY_VERIFICATION>",
                  "api": "http://sv-scanning-service:9082"
                },
                "menu": "[
                  {\"name\": \" ML Learning Path\",
                   \"url\": \"\",
                   \"imagePath\": \"/images/sidebar-icons/ML_learning_path_selected.png\" }]",
                "cloud_enabled": "[
                  { \"cloudEnabled\": \"true\", \"cloudName\": \"azure\",
                    \"cloudDisplayText\": \"Microsoft Azure\",
                    \"imageUrl\": \"/images/deploy-cloud/microsoft_azure.png\" },
                  { \"cloudEnabled\": \"false\", \"cloudName\": \"gcp\",
                    \"cloudDisplayText\": \"Google Cloud Platform\",
                    \"imageUrl\": \"/images/deploy-cloud/google_cloud_platform.png\" },
                  { \"cloudEnabled\": \"true\",  \"cloudName\": \"deploylocal\",
                    \"cloudDisplayText\": \"Deploy To Local\",
                    \"imageUrl\": \"/images/deploy-cloud/deploy_to_local.png\" },
                  { \"cloudEnabled\": \"false\", \"cloudName\": \"whitebox\",
                    \"cloudDisplayText\": \"Deploy To White Box\",
                    \"imageUrl\": \"/images/deploy-cloud/deploy_to_whitebox.png\" }]",
                "ds": {
                  "menu": "{
                    \"workbenchActive\": true,
                    \"acucomposeActive\": true,
                    \"blocks\": []}"
                }
              },
              "ui": {
                "server": {
                  "address": "https://<GRAPHENE_ORIGIN>"
                }
              },
              "submitValidation": {
                "api": "http://validation-client-service:9603/status/v1.0/tasks"
              },
              "dashboard": {
                "url": "http://<GRAPHENE_ELK_DOMAIN>:<GRAPHENE_ELK_KIBANA_PORT>"
              },
              "mailjet": {
                "api": {
                  "key": "<GRAPHENE_MAILJET_API_KEY>"
                },
                "secret": {
                  "key": "<GRAPHENE_MAILJET_SECRET_KEY>"
                },
                "address": {
                  "from": "<GRAPHENE_MAILJET_ADMIN_EMAIL>"
                }
              }
            },
            "spring": {
              "mail": {
                "host": "<GRAPHENE_SPRING_MAIL_SERVICE_DOMAIN>",
                "port": <GRAPHENE_SPRING_MAIL_SERVICE_PORT>,
                "username": "<GRAPHENE_SPRING_MAIL_USERNAME>",
                "password": "<GRAPHENE_SPRING_MAIL_PASSWORD>",
                "debug": "true",
                "smtp": {
                  "starttls": {
                    "enable": "<GRAPHENE_SPRING_MAIL_STARTTLS>"
                  },
                  "auth": "<GRAPHENE_SPRING_MAIL_AUTH>"
                },
                "transport": {
                  "protocol": "<GRAPHENE_SPRING_MAIL_PROTOCOL>"
                },
                "template": {
                  "folder": {
                    "path": "/fmtemplates/"
                  }
                }
              },
              "servlet": {
                "multipart": {
                  "max-file-size": "1024MB",
                  "max-request-size": "1024MB"
                }
              }
            },
            "client": {
              "ssl": {
                "key-store": "/app/certs/<GRAPHENE_KEYSTORE_P12>",
                "key-store-password": "<GRAPHENE_KEYSTORE_PASSWORD>",
                "key-store-type": "PKCS12",
                "key-password": "<GRAPHENE_CERT_KEY_PASSWORD>",
                "trust-store": "/app/certs/<GRAPHENE_TRUSTSTORE>",
                "trust-store-password": "<GRAPHENE_TRUSTSTORE_PASSWORD>"
              }
            },
            "gateway": {
              "url": "https://<GRAPHENE_FEDERATION_DOMAIN>:<GRAPHENE_FEDERATION_LOCAL_PORT>"
            },
            "dcae": {
              "model": {
                "name": {
                  "prefix": "ONAP"
                }
              }
            },
            "jwt": {
              "auth": {
                "secret": {
                  "key": "<GRAPHENE_JWT_KEY>"
                },
                "timeout": "80000000"
              }
            },
            "cas": {
              "service": {
                "validate": {
                  "url": "https://ecas.ec.europa.eu/cas/p3/serviceValidate"
                }
               }
              }
            },
            "logging": {
              "level":  {
                "org":   {
                  "graphene": {
                    "securityverification": "INFO",
                    "portal": "INFO"
                  }
                }
              }
            },
            "document": {
              "size": "<GRAPHENE_PORTAL_DOCUMENT_MAX_SIZE>"
            },
            "jupyter": {
              "url": "https://<GRAPHENE_ORIGIN>/hub/"
            },
            "kubernetes": {
               "doc": {
                  "url": "https://gitlab.eclipse.org/eclipse/graphene/tutorials"
               }
            },
            "image": {
              "size": "<GRAPHENE_PORTAL_IMAGE_MAX_SIZE>"
            }
          }'
        ports:
        - containerPort: 8083
        volumeMounts:
        - mountPath: /maven/logs
          name: logs
        - mountPath: /app/certs
          name: certs-volume
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
          claimName: <GRAPHENE_PORTAL_SERVICE_LABEL>
      - name: certs-volume
        configMap:
          name: graphene-certs

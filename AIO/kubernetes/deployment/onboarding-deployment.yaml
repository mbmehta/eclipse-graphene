apiVersion: apps/v1
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================

# What this is: kubernetes template for Graphene onboarding deployment
# How to use:

kind: Deployment
metadata:
  namespace: <GRAPHENE_NAMESPACE>
  name: onboarding
  labels:
    app: onboarding
spec:
  replicas: 1
  selector:
    matchLabels:
      app: onboarding
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: onboarding
        <GRAPHENE_SERVICE_LABEL_KEY>: <GRAPHENE_ONBOARDING_SERVICE_LABEL>
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: <GRAPHENE_SERVICE_LABEL_KEY>
                operator: NotIn
                values:
                - <GRAPHENE_ONBOARDING_SERVICE_LABEL>
            topologyKey: "kubernetes.io/node"
      imagePullSecrets:
      - name: graphene-registry
      containers:
      - name: onboarding
        image: <ONBOARDING_IMAGE>
        # TODO: assess why privilege is needed in Centos to avoid errors ala '(No such file or directory)'
        securityContext:
          privileged: <GRAPHENE_PRIVILEGED_ENABLE>
        command: ["/bin/sh", "-c"]
        args:
        - set -x;
          cd maven;
          java $JAVA_OPTS -Dhttp.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttp.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttp.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Dhttps.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttps.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttps.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Djava.security.egd=file:/dev/./urandom -jar *.jar
        env:
        - name: GRAPHENE_HTTP_NON_PROXY_HOSTS
          value: "<GRAPHENE_HTTP_NON_PROXY_HOSTS>|docker-dind-service|portal-be-service|msg-service|sv-scanning-service"
        - name: GRAPHENE_HTTP_PROXY_HOST
          value: "<GRAPHENE_HTTP_PROXY_HOST>"
        - name: GRAPHENE_HTTP_PROXY_PORT
          value: "<GRAPHENE_HTTP_PROXY_PORT>"
        - name: JAVA_OPTS
          value: "-Xms256m -Xmx2048m"
        - name: SPRING_APPLICATION_JSON
          value: '{
            "logging": {
              "level": {
                "root": "INFO"
              }
            },
            "server":{
              "port": 8090,
              "servlet": {
                "context-path": "/onboarding-app"
              }
            },
            "docker": {
              "host": "<GRAPHENE_DOCKER_API_HOST>",
              "port": "<GRAPHENE_DOCKER_API_PORT>",
              "config": "/docker_host/.docker",
              "registry":{
                "url": "http://<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT>/",
                "username": "<GRAPHENE_DOCKER_REGISTRY_USER>",
                "password": "<GRAPHENE_DOCKER_REGISTRY_PASSWORD>",
                "email": "<GRAPHENE_ADMIN_EMAIL>"
              },
              "tls": {
                "verify": "false"
              },
              "api": {
                "version": "1.23"
              },
              "imagetag": {
                "prefix": "<GRAPHENE_DOCKER_REGISTRY_HOST>:<GRAPHENE_DOCKER_MODEL_PORT><GRAPHENE_DOCKER_IMAGETAG_PREFIX>",
                "proxyPrefix": "<GRAPHENE_DOCKER_PROXY_HOST>:<GRAPHENE_DOCKER_PROXY_PORT><GRAPHENE_DOCKER_IMAGETAG_PREFIX>"
              },
              "max_total_connections": "1",
              "max_per_route_connections": "1"
            },
            "http_proxy": "<GRAPHENE_HTTP_PROXY>",
            "nexus": {
              "nexusEndPointURL": "http://<GRAPHENE_NEXUS_HOST>:<GRAPHENE_NEXUS_API_PORT>/<GRAPHENE_NEXUS_MAVEN_REPO_PATH>/<GRAPHENE_NEXUS_MAVEN_REPO>/",
              "nexusUserName": "<GRAPHENE_NEXUS_RW_USER>",
              "nexusPassword": "<GRAPHENE_NEXUS_RW_USER_PASSWORD>",
              "nexusproxy": "<GRAPHENE_HTTP_PROXY>",
              "nexusGroupId": "<GRAPHENE_NEXUS_GROUP>"
            },
            "cmndatasvc": {
              "cmnDataSvcEndPoinURL": "http://<GRAPHENE_CDS_HOST>:<GRAPHENE_CDS_PORT>/ccds",
              "cmnDataSvcUser": "<GRAPHENE_CDS_USER>",
              "cmnDataSvcPwd": "<GRAPHENE_CDS_PASSWORD>"
            },
            "mktPlace": {
              "mktPlaceEndPointURL": "http://portal-be-service:8083"
            },
            "microService": {
              "microServiceEndPointURL": "http://msg-service:8336/microservice-generation",
              "microServiceAsyncFlag": "false"
            },
            "requirements": {
              "extraIndexURL": "",
              "trustedHost": ""
            },
            "base_image": {
              "rimage": "<ONBOARDING_BASE_IMAGE>",
              "dockerusername": "<GRAPHENE_PROJECT_NEXUS_USERNAME>",
              "dockerpassword": "<GRAPHENE_PROJECT_NEXUS_PASSWORD>"
            },
            "tosca": {
              "OutputFolder": "/tmp/",
              "GeneratorEndPointURL": "http://delete-me/model_create"
            },
            "spring": {
              "servlet": {
                "multipart": {
                   "max-file-size": "1024MB",
                   "max-request-size": "1024MB"
                }
              }
            },
            "security": {
              "verificationEnableFlag": "<GRAPHENE_ENABLE_SECURITY_VERIFICATION>",
              "verificationApiUrl":"http://sv-scanning-service:9082/scan"
            }
          }'
        ports:
        - containerPort: 8090
        volumeMounts:
        - mountPath: /maven/logs
          name: logs
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
          claimName: <GRAPHENE_ONBOARDING_SERVICE_LABEL>

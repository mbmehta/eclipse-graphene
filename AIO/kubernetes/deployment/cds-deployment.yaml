apiVersion: apps/v1
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================

# What this is: kubernetes template for Graphene common-data-svc deployment
# How to use:

kind: Deployment
metadata:
  namespace: <GRAPHENE_NAMESPACE>
  name: cds
  labels:
    app: cds
spec:
  replicas: 1
  selector:
    matchLabels:
      app: cds
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: cds
        <GRAPHENE_SERVICE_LABEL_KEY>: <GRAPHENE_COMMON_DATA_SERVICE_LABEL>
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: <GRAPHENE_SERVICE_LABEL_KEY>
                operator: NotIn
                values:
                - <GRAPHENE_COMMON_DATA_SERVICE_LABEL>
            topologyKey: "kubernetes.io/node"
      imagePullSecrets:
      - name: graphene-registry
      containers:
      - name: cds
        image: <COMMON_DATASERVICE_IMAGE>
        command: ["/bin/sh", "-c"]
        args:
        - set -x;
          cd maven;
          java $JAVA_OPTS -Dhttp.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttp.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttp.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Dhttps.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttps.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttps.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Djava.security.egd=file:/dev/./urandom -jar *.jar
        env:
        - name: GRAPHENE_HTTP_NON_PROXY_HOSTS
          value: "<GRAPHENE_HTTP_NON_PROXY_HOSTS>|<GRAPHENE_MARIADB_HOST>"
        - name: GRAPHENE_HTTP_PROXY_HOST
          value: "<GRAPHENE_HTTP_PROXY_HOST>"
        - name: GRAPHENE_HTTP_PROXY_PORT
          value: "<GRAPHENE_HTTP_PROXY_PORT>"
        - name: JAVA_OPTS
          value: "-Xms128m -Xmx1024m"
        - name: SPRING_APPLICATION_JSON
          value: '{
            "logging": {
              "level": {
                "root": "INFO"
              }
            },
            "server": {
              "port": 8000,
              "connection-timeout": 900000
            },
            "spring": {
              "datasource": {
                "jdbc-url": "jdbc:mariadb://<GRAPHENE_MARIADB_HOST>:<GRAPHENE_MARIADB_PORT>/<GRAPHENE_CDS_DB>?useLegacyDatetimeCode=false&useSSL=false",
                "username": "<GRAPHENE_MARIADB_USER>",
                "password": "<GRAPHENE_MARIADB_USER_PASSWORD>"
              },
              "jpa": {
                "database-platform": "org.hibernate.dialect.MariaDB102Dialect",
                "hibernate": {
                  "ddl-auto": "validate"
                },
                "show-sql" : false
              },
              "security": {
                "user": {
                  "name": "<GRAPHENE_CDS_USER>",
                  "password": "<GRAPHENE_CDS_PASSWORD>"
                }
              }
            }
          }'
        ports:
        - containerPort: 8000
        volumeMounts:
        - mountPath: /maven/logs
          name: logs
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
         claimName: <GRAPHENE_COMMON_DATA_SERVICE_LABEL>

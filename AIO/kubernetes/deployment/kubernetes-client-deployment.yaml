apiVersion: apps/v1
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================

# What this is: kubernetes template for Graphene kubernetes-client deployment
# How to use:

kind: Deployment
metadata:
  namespace: <GRAPHENE_NAMESPACE>
  name: kubernetes-client
  labels:
    app: kubernetes-client
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kubernetes-client
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: kubernetes-client
        <GRAPHENE_SERVICE_LABEL_KEY>: <GRAPHENE_KUBERNETES_CLIENT_SERVICE_LABEL>
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: <GRAPHENE_SERVICE_LABEL_KEY>
                operator: NotIn
                values:
                - <GRAPHENE_KUBERNETES_CLIENT_SERVICE_LABEL>
            topologyKey: "kubernetes.io/node"
      imagePullSecrets:
      - name: graphene-registry
      containers:
      - name: kubernetes-client
        image: <KUBERNETES_CLIENT_IMAGE>
        command: ["/bin/sh", "-c"]
        args:
        - set -x;
          cd maven;
          java $JAVA_OPTS -Dhttp.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttp.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttp.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Dhttps.proxyHost=$GRAPHENE_HTTP_PROXY_HOST -Dhttps.proxyPort=$GRAPHENE_HTTP_PROXY_PORT -Dhttps.nonProxyHosts=$GRAPHENE_HTTP_NON_PROXY_HOSTS -Djava.security.egd=file:/dev/./urandom -jar *.jar
        env:
        - name: GRAPHENE_HTTP_NON_PROXY_HOSTS
          value: "<GRAPHENE_HTTP_NON_PROXY_HOSTS>|cds-service"
        - name: GRAPHENE_HTTP_PROXY_HOST
          value: "<GRAPHENE_HTTP_PROXY_HOST>"
        - name: GRAPHENE_HTTP_PROXY_PORT
          value: "<GRAPHENE_HTTP_PROXY_PORT>"
        - name: JAVA_OPTS
          value: "-Xms128m -Xmx512m"
        - name: SPRING_APPLICATION_JSON
          value: '{
            "logging": {
              "level": {
                "root": "INFO"
              }
            },
            "kube" : {
              "incrementPort": "8557",
              "singleModelPort": "8556",
              "folderPath": "/maven/home",
              "singleNodePort": "30333",
              "singleTargetPort": "8061",
              "dataBrokerModelPort": "8556",
              "dataBrokerNodePort": "30556",
              "dataBrokerTargetPort": "8556",
              "mlTargetPort": "8061",
              "nginxImageName": "nginx",
              "nexusEndPointURL": "http://localhost:80"
            },
            "dockerproxy": {
            "host": "<GRAPHENE_DOCKER_PROXY_HOST>",
            "port": "<GRAPHENE_DOCKER_PROXY_PORT>"
            },
            "blueprint": {
              "ImageName": "<BLUEPRINT_ORCHESTRATOR_IMAGE>",
              "name": "blueprint-orchestrator",
              "nodePort": "30555",
              "port": "8061"
            },
            "nexus": {
              "url": "http://<GRAPHENE_NEXUS_HOST>:<GRAPHENE_NEXUS_API_PORT>/<GRAPHENE_NEXUS_MAVEN_REPO_PATH>/<GRAPHENE_NEXUS_MAVEN_REPO>/",
              "password": "<GRAPHENE_NEXUS_RW_USER_PASSWORD>",
              "username": "<GRAPHENE_NEXUS_RW_USER>",
              "groupid": "<GRAPHENE_NEXUS_GROUP>"
            },
            "cmndatasvc": {
              "cmndatasvcendpointurl": "http://<GRAPHENE_CDS_HOST>:<GRAPHENE_CDS_PORT>/ccds",
              "cmndatasvcuser": "<GRAPHENE_CDS_USER>",
              "cmndatasvcpwd": "<GRAPHENE_CDS_PASSWORD>"
            },
            "probe": {
              "probeImageName": "<PROTO_VIEWER_IMAGE>",
              "probeImagePORT": "5006",
              "probeModelPort": "5006",
              "probeNodePort": "30800",
              "probeTargetPort": "5006",
              "probeApiPort": "5006",
              "probeExternalPort": "30800",
              "probeSchemaPort": "80"
            },
            "logstash": {
              "host": "<GRAPHENE_ELK_HOST>",
              "ip": "<GRAPHENE_ELK_HOST_IP>",
              "port": "<GRAPHENE_ELK_LOGSTASH_PORT>"
            },
            "server": { "port": "8082" }
          }'
        ports:
        - containerPort: 8082
        volumeMounts:
        - mountPath: /maven/logs
          name: logs
      restartPolicy: Always
      volumes:
      - name: logs
        persistentVolumeClaim:
         claimName: <GRAPHENE_KUBERNETES_CLIENT_SERVICE_LABEL>

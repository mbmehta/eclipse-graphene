#source ../graphene_env.sh

# check if the this script is called in the context of renew-letsencrypt or installation
if [ -z "$GRAPHENE_USER" ]; then
  # we are in installation context
  sudo cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/privkey.pem graphene.key
  sudo chown $USER:$USER graphene.key
  sudo cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/fullchain.pem graphene.crt
  sudo chown $USER:$USER graphene.crt
fi
rm -f *store*
sed -i '/STORE/d' cert_env.sh

KEYSTORE_PASSWORD=$(uuidgen)
echo "export KEYSTORE_PASSWORD=$KEYSTORE_PASSWORD" >>cert_env.sh
openssl pkcs12 -export \
  -in $GRAPHENE_CERT \
  -inkey $GRAPHENE_CERT_KEY \
  -passin pass:$CERT_KEY_PASSWORD \
  -certfile $GRAPHENE_CERT \
  -out $GRAPHENE_CERT_PREFIX-keystore.p12 \
  -passout pass:$KEYSTORE_PASSWORD

TRUSTSTORE_PASSWORD=$(uuidgen)
echo "export TRUSTSTORE_PASSWORD=$TRUSTSTORE_PASSWORD" >>cert_env.sh
keytool -import \
  -file $GRAPHENE_CERT \
  -alias $GRAPHENE_CERT_PREFIX-ca \
  -storetype JKS -keystore $GRAPHENE_CERT_PREFIX-truststore.jks \
  -storepass $TRUSTSTORE_PASSWORD -noprompt


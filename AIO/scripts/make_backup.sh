#!/bin/bash

set -x
trap 'fail' ERR

export AIO_ROOT=$HOME/eclipse-graphene/AIO
source $AIO_ROOT/utils.sh
source $AIO_ROOT/graphene_env.sh

export TODAY=$(date +"%a")
export TARGET_BDIR=$HOME/backups/$TODAY
export BDIR=$HOME/backups/current
mkdir -p $BDIR
cd $BDIR
cp $AIO_ROOT/nexus_env.sh .


mysqldump -h $GRAPHENE_MARIADB_DOMAIN -P $GRAPHENE_MARIADB_NODEPORT --user=root --password=$GRAPHENE_MARIADB_PASSWORD  $GRAPHENE_CDS_DB > graphenedb.sql

export NEXUS_DATA_DIR=/mnt/graphene/$(kubectl -n $GRAPHENE_NEXUS_NAMESPACE get pvc | tail -1 | sed 's/ \+/ /g' | cut -d' ' -f3)
cd $NEXUS_DATA_DIR
tar -czf $BDIR/nexus-data.tgz --exclude=cache --exclude=tmp --exclude=log --exclude=javaprefs .

cd $HOME
rm -rf $TARGET_BDIR
mv $BDIR $TARGET_BDIR
